Provides options for users to make their nodeprofile fields private.
This module is based on the profile_privacy module - but this module works for profiles built using the nodeprofile module, rather than the profile.module

INSTALL
--------------------------------------------------------------------------------
- Drop the entire profile_privacy directory into your modules directory:
    Drupal 5+ - sites/all/modules or sites/my_site/modules

- Enable the module:
    Drupal 5+ - admin/build/modules



CONFIGURATION
--------------------------------------------------------------------------------
- This module does not have a settings page or central configuration. All additional features are made to existing forms.

- When installed, all the fields for a content type defined as a nodeprofile will have a tickbox by which administrators can control which fields users can make private
- Users can then edit their nodeprofile fields and show/hide their fields

CAVEATS
--------------------------------------------------------------------------------
- This module only works with the nodeprofile module - not the profile module. If you want to show/hide profile fields, then use the profile_privacy module

AUTHOR:
-------------
Ben Scott
ben@benscott.co.uk
